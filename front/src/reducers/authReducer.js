const authReducer = (state={}, action) => {
    const { user, token, message } = action;
    switch (action.type) {
        case "CREATE_SESSION":
            return { user, token, message };
        case "DELETE_MESSAGE":
            return { ...state, message: "" };
        default:
            return state;
    }
};

export default authReducer;