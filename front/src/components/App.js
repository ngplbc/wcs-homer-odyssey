import React from "react";
import { connect } from "react-redux";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import SignUp from "../containers/SignUp";
import SignIn from "../containers/SignIn";
import Profile from "../containers/Profile";
import requireAuth from "../hoc/requireAuth";
import requireNotAuth from "../hoc/requireNotAuth";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { Grid, Paper, Snackbar } from "@material-ui/core";


const mapStateToProps = (state) => ({
    token: state.auth.token,
    flash: state.auth.message,
});

const mapDispatchToProps = (dispatch) => ({
    handleClose: () => dispatch({ type: "DELETE_MESSAGE" })
});

class App extends React.Component {
    render() {
        return (
            <div className="App">
                <MuiThemeProvider>
                    <Grid
                        container
                        alignItems="center"
                        style={{ height: "100%" }}>
                        <Grid item xs={12}>
                            <Paper elevation={4} style={{ margin: 32 }}>
                                <Grid
                                    container
                                    alignItems="center"
                                    justify="center">
                                    <Grid
                                        item
                                        xs={12}
                                        sm={6}
                                        style={{ textAlign: "center" }}>
                                        <img
                                            src="http://images.innoveduc.fr/react_odyssey_homer/wildhomer.png"
                                            alt=""
                                        />
                                    </Grid>
                                    <Grid
                                        item
                                        xs={12}
                                        sm={6}
                                        alignContent="center">
                                        <BrowserRouter>
                                        <Switch>
                                            <Redirect exact from="/" to="/profile" />
                                            <Route exact path="/profile" component={requireAuth(Profile)} />
                                            <Route exact path="/signin" component={requireNotAuth(SignIn)} />
                                            <Route exact path="/signup" component={requireNotAuth(SignUp)} />
                                        </Switch>
                                        </BrowserRouter>
                                    </Grid>
                                </Grid>
                            </Paper>
                        </Grid>
                    </Grid>
                </MuiThemeProvider>
                {this.props.flash && (
                    <Snackbar
                        open={this.props.flash.length ? true : false}
                        message={this.props.flash}
                        role="alert"
                        onClose={this.props.handleClose}
                        autoHideDuration={2000}
                    />
                )}
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
