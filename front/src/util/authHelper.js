export const signUpHelper = async (user) => {
    const options = {
        headers: {
            "Content-Type": "application/json"
        },
        method: "POST",
        body: JSON.stringify(user)
    };
    const rawResponse = await fetch("/auth/signup", options);
    const response = await rawResponse.json();
    console.log(response);
    return response;
};

export const signInHelper = async (user) => {
    const options = {
        headers: {
            "Content-Type": "application/json"
        },
        method: "POST",
        body: JSON.stringify(user)
    };
    const rawResponse = await fetch("/auth/signin", options);
    const response = await rawResponse.json();
    console.log(response);
    return response;
};

