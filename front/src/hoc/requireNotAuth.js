import React, { useEffect } from "react";
import { connect } from "react-redux";

const requireNotAuth = (Component) => {
    const mapStateToProps = (state) => ({
        authenticated: state.auth.token ? true : false
    })
    
    const Authentication = (props) => {
        useEffect(() => {
            if (props.authenticated) {
                props.history.push("/profile");
            } 
        })

        return <Component {...props} />
    }

    return connect(mapStateToProps)(Authentication);
};

export default requireNotAuth;