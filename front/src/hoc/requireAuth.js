import React, { useEffect } from "react";
import { connect } from "react-redux";

const requireAuth = (ComposedComponent) => {
    const mapStateToProps = (state) => ({
        authenticated: state.auth.token ? true : false
    });
    
    const Authentication = (props) => {
        useEffect(() => {
            if (!props.authenticated)  {
                props.history.push("/signin");
            } 
        })
        
        return <ComposedComponent {...props} />
    };

    return connect(mapStateToProps)(Authentication);
};

export default requireAuth;