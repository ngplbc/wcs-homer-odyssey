import React from "react";
import { connect } from "react-redux";
import { TextField, Button } from "@material-ui/core";
import { Link, withRouter } from "react-router-dom";
import { signInHelper } from "../util/authHelper";


const mapStateToProps = state => ({ 
    flash: state.auth.token 
});

class SignIn extends React.Component {
    state = {
        email: "test@mail.com",
        password: "0"
    };

    onSubmitHandler = async (e) => {
        e.preventDefault();
        const response = await signInHelper(this.state);
        this.props.dispatch({
            type: "CREATE_SESSION",
            user: response.user,
            token: response.token,
            message: response.message,
        })
        this.props.history.replace("/");
    };

    render() {
        const { email, password } = this.state;
        return (
            <>
                <h3>Sign In!</h3>
                <form onSubmit={this.onSubmitHandler}>
                    <div>
                        <TextField
                            label="Email"
                            fullWidth
                            onChange={e =>
                                this.setState({ email: e.target.value })
                            }
                            type="email"
                            value={email}
                        />
                    </div>
                    <div>
                        <TextField
                            label="Password"
                            fullWidth
                            onChange={e =>
                                this.setState({ password: e.target.value })
                            }
                            type="password"
                            value={password}
                        />
                    </div>
                    <Button
                        id="submit"
                        type="submit"
                        variant="contained"
                        color="primary">
                        Submit
                    </Button>
                    <Link to="/signup">Sign Up</Link>
                </form>
            </>
        );
    }
}

export default withRouter(connect(mapStateToProps)(SignIn));
