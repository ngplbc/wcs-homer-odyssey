import React from "react";
import { connect } from "react-redux";
import { List, ListItem, ListItemText } from "@material-ui/core";
import { Link } from "react-router-dom";


const mapStateToProps = (state) => ({
    profile: state.auth.user 
})

class Profile extends React.Component {
    render() { 
        return (
            <>
                <List>
                    <ListItem>
                        <ListItemText primary={this.props.profile ? this.props.profile.firstName : null} secondary={this.props.profile ? this.props.profile.email : null} />
                    </ListItem>
                </List>
                <Link to="/">Sign Out</Link>
            </>
        );
    }
}

export default connect(mapStateToProps)(Profile);
