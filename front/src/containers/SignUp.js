import React from "react";
import { TextField, Button } from "@material-ui/core";
import { Link, withRouter } from "react-router-dom";
import { signUpHelper } from "../util/authHelper";


class SignUp extends React.Component {
    state = {
        email: "",
        password: "",
        passwordConf: "",
        firstName: "",
        lastName: ""
    };

    onSubmitHandler = async e => {
        e.preventDefault();
        const response = await signUpHelper(this.state);
        this.props.history.push("/");
    };

    render() {
        const {
            email,
            password,
            passwordConf,
            firstName,
            lastName,
        } = this.state;
        return (
            <>
                <h3>Sign Up!</h3>
                <form onSubmit={this.onSubmitHandler}>
                    <div>
                        <TextField
                            label="Email"
                            fullWidth
                            onChange={e =>
                                this.setState({ email: e.target.value })
                            }
                            type="email"
                            value={email}
                        />
                    </div>
                    <div>
                        <TextField
                            label="Password"
                            fullWidth
                            onChange={e =>
                                this.setState({ password: e.target.value })
                            }
                            type="password"
                            value={password}
                        />
                    </div>
                    <div>
                        <TextField
                            label="Password Copy"
                            fullWidth
                            onChange={e =>
                                this.setState({ passwordConf: e.target.value })
                            }
                            type="password"
                            value={passwordConf}
                        />
                    </div>
                    <div>
                        <TextField
                            label="Name"
                            fullWidth
                            onChange={e =>
                                this.setState({ firstName: e.target.value })
                            }
                            type="text"
                            value={firstName}
                        />
                    </div>
                    <div>
                        <TextField
                            label="Last name"
                            fullWidth
                            onChange={e =>
                                this.setState({ lastName: e.target.value })
                            }
                            type="text"
                            value={lastName}
                        />
                    </div>
                    <Button
                        id="submit"
                        type="submit"
                        variant="contained"
                        color="primary">
                        Submit
                    </Button>
                    <Link to="/">Sign in</Link>
                </form>
            </>
        );
    }
}

export default withRouter(SignUp);
