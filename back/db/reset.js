const connection = require("./conf");

connection.query(`DROP TABLE IF EXISTS user`, (err) => {
    if (err) console.log(err);
    else {
        connection.query(`CREATE TABLE user (
            id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
            email VARCHAR(255) UNIQUE NOT NULL,
            passwordHash VARCHAR(160) NOT NULL, 
            firstName VARCHAR(120) NOT NULL,
            lastName VARCHAR(120) NOT NULL
        )`, (err) => {
            if (err) console.log(err);
            else connection.end();
        })
    }
});