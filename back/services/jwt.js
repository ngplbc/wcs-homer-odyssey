const jwt = require("jsonwebtoken");
require("dotenv").config();

const genToken = (user) => jwt.sign(user, process.env.JWT_SECRET);

module.exports = { genToken };