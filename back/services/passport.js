const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const JWTStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const User = require("../models/User");
require("dotenv").config();

passport.use(
    new LocalStrategy(
        {
            usernameField: "email",
            passwordField: "password",
            session: false
        },
        (email, password, done) => {
            User.findOne(email, password, (err, user) => {
                if (err) return done(err);
                if (user) return done(null, user);
                return done(null, false, {
                    message: "Incorrect email or password."
                });
            });
        }
    )
);

passport.use(
    new JWTStrategy(
        {
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: process.env.JWT_SECRET
        },
        (jwtPayload, cb) => {
            return cb(null, jwtPayload);
        }
    )
);
