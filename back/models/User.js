const connection = require("../db/conf");
const bcrypt = require("bcrypt");

const User = {};

User.create = (userInfo, cb) => {
    const { email, password, firstName, lastName } = userInfo;
    const passwordHash = bcrypt.hashSync(password, 10);
    const sqlCreate = `INSERT INTO user (email, passwordHash, firstName, lastName) VALUES (?, ?, ?, ?)`;
    connection.query(sqlCreate, [email, passwordHash, firstName, lastName], (err, results, fields) => {
        cb(err, results);
    });
};

User.findOne = (email, password, cb) => {
    const sql = `SELECT * FROM user WHERE email = ?`;
    connection.query(sql, email, (err, results, fields) => {
        if (results.length) {
            const resultPassword = results[0].passwordHash;
            if (bcrypt.compareSync(password, resultPassword)) {
                return cb(err, results[0]);
            } 
        }
        return cb(err);
    });
};

module.exports = User;