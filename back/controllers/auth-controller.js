const passport = require("passport");
const User = require("../models/User");
const { genToken } = require("../services/jwt");

const createUser = (req, res, next) => {
    User.create(req.body.user, (err, results) => {
        if (err) return next(err);
        res.status(201).json({ message: "User has been signed up!" });
    })
};

const authenticateUser = (req, res) => {
    passport.authenticate("local", (err, user, info) => {
        if(err) return res.status(500).send(err)
        if (!user) return res.status(400).json({ message: info.message });
        const token = genToken(JSON.stringify(user));
        return res.status(200).json({ user, token, message: "You have logged in successfully" });
    })(req, res);
};


module.exports = { createUser, authenticateUser };