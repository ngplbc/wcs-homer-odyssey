const express = require("express");
const router = express.Router();
const { createUser, authenticateUser } = require("../controllers/auth-controller"); 

router.post("/signup", createUser);

router.post("/signin", authenticateUser);

module.exports = router;