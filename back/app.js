const http = require('http');
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require("cors");
const app = express();
const authRouter = require("./routes/authRouter");
require("dotenv").config();

const connection = require("./db/conf");

connection.connect((err) => {
  if (err) console.log(err);
  else console.log("Connected to Database");
})

// set up the application
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended:  false }));
app.use(bodyParser.json());
app.use(cors());
app.use(express.static(__dirname  +  '/public'));

// config passport
require("./services/passport");

app.use("/auth", authRouter);

// TODO: refactor
const passport = require("passport");
app.get("/profile", passport.authenticate("jwt", { session:  false }), (req, res, next) => {
  res.send({ email: req.user.email });
});

// implement the API part
app.get("/", (req,res) => {
  res.send("youhou");
})

// in case path is not found, return the 'Not Found' 404 code
app.use(function(req, res, next) {
  var  err  =  new  Error('Not Found');
  err.status  =  404;
  next(err);
});

// error handler
app.use((err, req, res, next) => {
  const status = err.status || 500;
  res.status(status).json({ flash: err.message });
})

// launch the node server
let  server  =  app.listen(process.env.PORT  ||  4000, function(){
  console.log('Listening on port '  +  server.address().port);
});